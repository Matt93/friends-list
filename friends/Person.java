package friends;

public class Person {
	String name;		// the person's name
	Friend firstFriend; // the first friend in the list of this
					    // person's friends
	Person nextPerson;  // the next person in the list of people
	
	public Person(String name, Person nextPerson) {
		this.name = name;
		this.nextPerson = nextPerson;
		}

	// A string representing the list of friends of this person.  
	// O(number of friends in the list)
	public String friendString(){
		String result = "";
		if (firstFriend == null){
			result = "No friends";
		}
		for (Friend ptr = firstFriend; ptr != null; ptr = ptr.nextFriend){
			if (ptr.nextFriend == null){
				result = result + ptr.who.name;
			}else{
				result = result + ptr.who.name + " ";
			}
		}
		return result;
	
	}
	
	// add friend as a friend of this person
	// O(1)
	public void addFriend(Person friend){
		if (firstFriend == null){
			firstFriend = new Friend(friend, null);
		}else{
			firstFriend.nextFriend = new Friend(friend, firstFriend.nextFriend);
		}
	}
	
	// remove Person friend as a friend of this person
	// if friend is not a friend of this person, does nothing
	// O(number of friends of this person)
	public void removeFriend(Person friend){
		if (firstFriend == null){
			return;
		}else if (firstFriend.who == friend){
			firstFriend = firstFriend.nextFriend;
			return;
		}
		
		Friend ptr;
		for (ptr = firstFriend; ptr != null; ptr = ptr.nextFriend){
			if (ptr.nextFriend.who == friend){
				if (friend.nextPerson == null){
					ptr.nextFriend = null;
					return;
				}else{
					ptr.nextFriend = ptr.nextFriend.nextFriend;
					return;
				}
			}
		}
	return;
	}
}
